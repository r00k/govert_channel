module gitlab.com/r00k/govert_channel

go 1.20

require (
    golang.org/x/net v0.15.0
    golang.org/x/sys v0.12.0 // indirect
)