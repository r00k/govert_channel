# govert_channel

Libraries for generating covert channels in go. Very little/no error checking and input validation, not for production at this time. 
Will be refactoring after initial functionality is complete. Will be working on packaging detection methods with covert channels once 
my home lab is complete.